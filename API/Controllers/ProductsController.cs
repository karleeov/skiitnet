using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers

{
    [ApiController]
    [Route("karl/[controller]")]
    public class ProductController : ControllerBase
    {
        public readonly IProductRepository _repo;

        public ProductController(IProductRepository repo)
        {
            _repo = repo;
        }
[HttpGet]

public async Task<ActionResult<List<Product>>> GetProducts() 
{
    var products = await _repo.GetProductsAsync();

    return Ok(products);
}
[HttpGet("{id}")]
public async Task<ActionResult<Product>> GetProduct(int id)
{
    return await _repo.GetProductById(id);

}
    }
}